<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'axxi_play_add_gateway_class' );
function axxi_play_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_AxxiPlay_Gateway'; // your class name is here
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'axxi_play_init_gateway_class' );
function axxi_play_init_gateway_class() {

	class WC_AxxiPlay_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'axxi_play'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'Axxi Play Gateway';
            $this->method_description = 'Payment for Wordpress use Axxi Play'; // will be displayed on the options page

        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

 		}
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Axxi Play Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Axxi Play',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via Axxi Play',
                ),
                'configPayment' => array(
                    'title'       => 'Config Payment',
                    'type'        => 'tag',
                    'text'        => "Your config of payment, contact an advisor ",
                ),
                'client_key' => array(
                    'title'       => 'Client key',
                    'label'       => 'Client key',
                    'type'        => 'text',
                ),
                'password' => array(
                    'title'       => 'Password',
                    'type'        => 'password',
                ),
            );
	 	}
         /**
          * Custon field tag
          */
        public function generate_tag_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top" class="tag">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <style>
                .tag{
                    background: #1d2327;
                    color: white;
                    box-shadow: -50px 0 #1d2327,50px 0 #1d2327;
                }
                .tag label{
                    color: white !important;
                }
            </style>
            <?php
            return ob_get_clean();
        }
         /**
          * Custon field info
          */
        public function generate_info_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
                // ok, let's display some description before the payment form
                if ( $this->description ) {
                    // you can instructions for test mode, I mean test card numbers etc.
                    // display the description with <p> tags etc.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
                ?>
                <div id="AXXI_content_cc" class="AXXI_content">
                    <div class="form-row form-row-wide " >
                        <label>
                            Card Number <span class="required">*</span>
                        </label>
                        <div class="AXXI_content_card_number">
                            <input id="AXXI_card_number" name="AXXI_card_number" type="text" autocomplete="off" required placeholder="Card Number">
                        </div>
                    </div>
                    <div class="form-row form-row-wide">
                        <label>
                            Card Email <span class="required">*</span>
                        </label>
                        <input id="AXXI_email" name="AXXI_email" type="email" autocomplete="off" required placeholder="Card Email">
                    </div>
                </div>
                <script>
                    var AXXI_LOG = <?=AXXI_LOG ? "true":"false"?> ;
                    var AXXI_URL = "<?=AXXI_URL?>";
                </script>
                <?php
		}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {
            if( empty( $_POST["AXXI_card_number"] )) {
                wc_add_notice(  'Card Name is required!', 'error' );
                return false;
            }
            if( empty( $_POST["AXXI_email"] )) {
                wc_add_notice(  'Card Email is required!', 'error' );
                return false;
            }
            return true;
		}
        public function generate_hash($email,$card)
        {
            $s = strrev($email).$this->get_option( 'password' ).strrev(substr($card,0,6).substr($card,-4));
            return md5($s);
            return hash("sha256",$s);
        }
        public function getApi($hash)
        {
            $api = new AXXI_api(array(
                "client_key"    =>  $this->get_option( 'client_key' ),
                "password"      =>  $this->get_option( 'password' ),
                "hash"          =>  $hash
            ));
            return $api;
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            try {
                global $woocommerce;
    
                $order = wc_get_order( $order_id );
    
                $card = $_POST["AXXI_card_number"];
                $email = $_POST["AXXI_email"];

                $hash = $this->generate_hash($email,$card);

                $api = $this->getApi($hash);

                $result = $api->sale();

                $order->payment_complete();
                $order->reduce_order_stock();
                $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                $woocommerce->cart->empty_cart();
    
                update_post_meta(
                    $order_id,
                    "payment",
                    "axxi_play"
                );
                update_post_meta(
                    $order_id,
                    "payment_axxi_play",
                    json_encode($result)
                );
                return array(
                    'result' => 'success',
                    'redirect' => $order->get_checkout_order_received_url()
                );	
            } catch (Exception $error) {
                wc_add_notice(  $error->getMessage(), 'error' );
                return false;
            }
	 	}
 	}
}