<?php
/*
Plugin Name: Axxi Play
Plugin URI: https://gitlab.com/franciscoblancojn/axxi-play
Description: This is a plugin for use payment Axxi.
Author: franciscoblancojn
Version: 1.0.1
Author URI: https://franciscoblanco.vercel.app/
License: ISC
 */
if (!function_exists( 'is_plugin_active' ))
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
if(!is_plugin_active( 'woocommerce/woocommerce.php' )){
    function AVSHME_log_dependencia() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p>
                Axxi Play requiere the plugin  "Woocommerce"
            </p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'AVSHME_log_dependencia' );
}else{
    require 'plugin-update-checker/plugin-update-checker.php';
    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://gitlab.com/franciscoblancojn/axxi-play',
        __FILE__,
        'axxi-play'
    );
    $myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
    $myUpdateChecker->setBranch('master');

    function AXXI_get_version() {
        $plugin_data = get_plugin_data( __FILE__ );
        $plugin_version = $plugin_data['Version'];
        return $plugin_version;
    }
    
    define("AXXI_LOG",false);
    define("AXXI_PATH",plugin_dir_path(__FILE__));
    define("AXXI_URL",plugin_dir_url(__FILE__));
    
    require_once AXXI_PATH . "src/_index.php";
}