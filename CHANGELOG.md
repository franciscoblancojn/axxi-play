# Changelog

All notable changes to this project will be documented in this file.

## [v1.0.1](https://gitlab.com/franciscoblancojn/axxi-play/-/tree/56c021f7325c5a3c7effa225b3b3f4b5e8d7ee74)

### New
- Api connect Axxi for Sale
- Payment Axxi for config your payment


## [v1.0.0](https://gitlab.com/franciscoblancojn/axxi-play/-/tree/6afa9076c68a4ccc190405e559b4c4c9cb87a5f6)

### New
- Init Plugin
