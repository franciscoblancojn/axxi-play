<?php


class AXXI_api
{
    private $client_key = "";
    private $hash = "";
    private $password = "";
    private $URL = "https://api.axxipay.com/post";
    

    public function __construct($settings = array())
    {
        $this->client_key   = $settings["client_key"];
        $this->hash         = $settings["hash"];
        $this->password     = $settings["password"];
    }

    private function request($json)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URL,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
        ));
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        $response = json_decode($response,true);

        return $response;
    }
    public function sale()
    {
        return $this->request(array(
            "client_key"    => $this->client_key,
            "hash"          => $this->hash,
            "password"      => $this->password,
        ));
    }
}
